# icreate-app

> 该项目建立于2018/1008，是Hybrid App（混合模式移动应用），介于web-app、native-app这两者之间的app，
兼具“Native App良好用户交互体验的优势”和“Web App跨平台开发的优势”。
该项目由vue项目集成于cordova框架，vue 编译后的文件生成到cordova项目的www目录，再利用cordova将其打包成apk或者ipa

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# install cordova
npm install -g cordova

#add browser platform for using cordvoa
cordova platform add browser

#add android platform for using cordvoa
cordova platform add android

#build android
#build the vue project and created the result files to www directory
1.npm run build 
#packaging the www diretory for using cordova 
2.cordova build android

```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
